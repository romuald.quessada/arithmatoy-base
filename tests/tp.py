# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    nombre_str = "0"
    while(n!=0):
        nombre_str = "S" +nombre_str
        n=n-1 
    return nombre_str


def S(n: str) -> str:
    return "S" + n
    


def addition(a: str, b: str) -> str:
    nbr_a = len(a) - 1
    nbr_b = len(b) - 1
    nbr_c = nbr_b + nbr_a
    return nombre_entier(nbr_c) 
    


def multiplication(a: str, b: str) -> str:
    nbr_a = len(a) - 1
    nbr_b = len(b) - 1
    nbr_c = nbr_a * nbr_b
    return nombre_entier(nbr_c)


def facto_ite(n: int) -> int:
    factorial = 1
    for i in range(1, n+1):
        factorial *= i
    return factorial



def facto_rec(n: int) -> int:
    if n == 0:
        return 1
    else:
        return n * facto_rec(n-1)


def fibo_rec(n: int) -> int:
    if n <= 1:
        return n
    else:
        return fibo_rec(n-1) + fibo_rec(n-2)


def fibo_ite(n: int) -> int:
    if n <= 1:
        return n

    fib_ancien = 0
    fib_mtn = 1

    for _ in range(2, n+1):
        fib_nxt = fib_ancien + fib_mtn
        fib_ancien = fib_mtn
        fib_mtn = fib_nxt

    return fib_mtn



def golden_phi(n: int) -> int:
    gold = 1
    for i in range(1, n+1):
        gold = 1 + 1 / gold
    return gold



def sqrt5(n: int) -> int:
    return n ** (0.2)


def pow(a: float, n: int) -> float:
    if n == 0:
        return 1.0
    elif n == 1:
        return a

    i = 0
    power = 1

    while i != n:
        power *= a
        i += 1

    return power
